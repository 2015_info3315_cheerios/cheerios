from flask import Flask, render_template, request
app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/quiz", methods=['GET', 'POST'])
def quiz():
    if request.method == 'POST':
        answers = [i for i in request.form]
        mark = 0
        for i in answers: # aababbbabb
            if int(i)== 1 and request.form[i] == "a":
                mark += 1
            elif int(i)== 2 and request.form[i] == "a":
                mark += 1
            elif int(i)== 3 and request.form[i] == "b":
                mark += 1
            elif int(i)== 4 and request.form[i] == "a":
                mark += 1
            elif int(i)== 5 and request.form[i] == "b":
                mark += 1
            elif int(i)== 6 and request.form[i] == "b":
                mark += 1
            elif int(i)== 7 and request.form[i] == "b":
                mark += 1
            elif int(i)== 8 and request.form[i] == "a":
                mark += 1
            elif int(i)== 9 and request.form[i] == "b":
                mark += 1
            elif int(i)== 10 and request.form[i] == "b":
                mark += 1
            print mark
        return render_template("quizresults.html", mark=mark)
    return render_template("quiz.html")

@app.route("/selfassessment", methods=['GET', 'POST'])
def self_assess():
        if request.method == 'POST':
            answers = [i for i in request.form]
            mark = 0
            for i in answers:
                if int(i)== 1 and request.form[i] == "a":
                    mark += 1
                elif int(i)== 2 and request.form[i] == "a":
                    mark += 1
                elif int(i)== 3 and request.form[i] == "a":
                    mark += 1
                elif int(i)== 4 and request.form[i] == "a":
                    mark += 1
                elif int(i)== 5 and request.form[i] == "a":
                    mark += 1
                elif int(i)== 6 and request.form[i] == "a":
                    mark += 1
                elif int(i)== 7 and request.form[i] == "a":
                    mark += 1
                elif int(i)== 8 and request.form[i] == "a":
                    mark += 1
                elif int(i)== 9 and request.form[i] == "a":
                    mark += 1
                elif int(i)== 10 and request.form[i] == "a":
                    mark += 1
                print mark
            return render_template("anxietylevel.html", mark=mark)
        return render_template("self-assessment.html")

@app.route("/contact")
def contact():
    pass

if __name__ == "__main__":
    app.debug = True
    app.run()
